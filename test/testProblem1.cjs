const problem1 = require('../problem1.cjs')
const assert = require('assert');


assert.deepEqual(problem1([{ "id": 0, "car_make": "Dodge", "car_model": "Magnum", "car_year": 2008 }], 0), { "id": 0, "car_make": "Dodge", "car_model": "Magnum", "car_year": 2008 })