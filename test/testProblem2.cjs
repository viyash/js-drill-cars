const problem = require('../problem2.cjs')
const assert = require('assert');

let data = [{"id":11,"car_make":"Infiniti","car_model":"G35","car_year":2004},
{"id":12,"car_make":"Lotus","car_model":"Esprit","car_year":2004},
{"id":13,"car_make":"Chevrolet","car_model":"Cavalier","car_year":1997},
{"id":14,"car_make":"Dodge","car_model":"Ram Van 1500","car_year":1999},
{"id":15,"car_make":"Dodge","car_model":"Intrepid","car_year":2000}]
const result = 'Last car is a Dodge Intrepid Car'

assert.equal(problem(data), result)