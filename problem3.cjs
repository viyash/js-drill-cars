const data = require('./data.cjs')


// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.



function problem3(data) {
    let keys = [];
    if (Array.isArray(data)) {
        if (data.length > 0) {
            for (let keyIndex = 1; keyIndex < data.length; keyIndex++) {
                keys.push(data[keyIndex].car_model)
            }
            keys.sort(function (a, b) { a = a.toLowerCase(); b = b.toLowerCase(); if (a == b) return 0; if (a > b) return 1; return -1; });
            return keys
        }
    }
}

const result = problem3(data)
console.log(result)

module.exports = problem3