const data = require('./data.cjs')


// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*"

function problem2(data) {
    
    if (Array.isArray(data)) {
        if (data.length > 0) {
            const index = data.length - 1
            return `Last car is a ${data[index].car_make} ${data[index].car_model} Car`
        }
    }
}

const result = problem2(data)
console.log(result)

module.exports = problem2
