const data = require('./data.cjs')
let problem4 = require('./problem4.cjs')


// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function problem5(data) {
    let count = 0;
    if (Array.isArray(data)) {
        if (data.length > 0) {
            for (let keyIndex = 0; keyIndex < data.length; keyIndex++) {
                let year = data[keyIndex]
                if (year > 2000) {
                    count += 1
                }
            }
        }
    }
    return count
}

let year_data = problem4(data)
const result = problem5(year_data)
console.log(result)

module.exports = problem5
